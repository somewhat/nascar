import React, { Component } from 'react';
import home_slide_1 from './images/home-slide-1.png';
import mob_home_img from './images/mob-home-img.png';
import appstore from './images/appstore.png';
import googleplay from './images/googleplay.png';


class TopBanner extends Component {
   handleClick = (e) => {
    document.getElementById('videopopup').classList.add('show');
	document.getElementById('bannervideo').setAttribute('src', "https://www.youtube.com/embed/-u4h24Vz940");
	
  }
  videoWrapClick = (e) => {
    document.getElementById('videopopup').classList.remove('show');
	document.getElementById('bannervideo').setAttribute('src', "");
  }
  render() {
    return (
	<div className="container-fluid">
	  <div className="row">
		<div id="homeslider">
		  <div>
			<div className="desktop"> 
				<img className="slideimg desktop" src={home_slide_1} alt="..." />
			  <div className="carousel-caption d-none d-md-block slidecaption">
				<h2>The one financial<br /> membership that<br />  leaves all others <br />in the dust.</h2>
				<p>MoneyLion Racing and Team <br />Penske bring you free tickets, <br />cash back and more.</p>
				<div className="onlinestore"> <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={appstore} alt="appstore" /></a> <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={googleplay} alt="googleplay" /></a> </div>
			 </div>
			</div>
			<div className="mob-caption slidecaption mob-home-wrap">
				<h2>The one financial<br /> membership that<br /> leaves all others <br />in the dust.</h2>
				<div className="mob-home-desc">
					
					<span>MoneyLion Racing and Team <br />Penske bring you free tickets, <br />cash back and more.</span>
				</div>				                
				<div className="mob-home-img">
					<img src={mob_home_img} className="mob-home-image"  alt=""/>
				<div className="onlinestore"> <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer"><img src={appstore} alt="appstore"  /></a> 
				<a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank"  rel="noopener noreferrer"><img src={googleplay} alt="googleplay"  /></a> </div>
				</div>
			</div>
			<div id="videopopup" className="modal " onClick={this.videoWrapClick}>
				<div className="modal-dialog">
					<div className="modal-content">
						<div className="modal-body">
							<iframe title="frame-2" id="bannervideo" width="560" height="315" src="" frameBorder="0" allowFullScreen></iframe>
						</div>
					</div>
				</div>
			</div>
		  </div>
		</div>
		  
	  </div>
	</div>
    );
  }
}


export default TopBanner;
