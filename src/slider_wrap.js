import React, { Component } from 'react';
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

import bank_slide_1 from './images/Bank-Slide-1.png';
import bank_slide_1_mob from './images/Bank-Slide-1-mob.png';
import bank_slide_2 from './images/Bank-Slide-2.png';
import bank_slide_2_mob from './images/Bank-Slide-2-mob.png';
import bank_slide_3 from './images/Bank-Slide-3.png';
import bank_slide_3_mob from './images/Bank-Slide-3-mob.png';
import bank_slide_4 from './images/Bank-Slide-4.png';
import bank_slide_4_mob from './images/Bank-Slide-4-mob.png';
import bank_slide_5 from './images/Bank-Slide-5.png';
import bank_slide_5_mob from './images/Bank-Slide-5-mob.png';
import bank_slide_6 from './images/Bank-Slide-6.png';
import bank_slide_6_mob from './images/Bank-Slide-6-mob.png';
import arrow_left from './images/ArrowLeft.png';
import arrow_right from './images/ArrowRight.png';

class SliderWrap extends Component {
  render() {
	  const settings = {
      dots: false,
      infinite: true,
      speed: 1000,
	  autoplaySpeed:2000,
      slidesToShow: 1,
      slidesToScroll: 1,
    };
    return (
	<div className="slider-section py-4">
	  <div className="container">
		<div className="desktop">
			<div id="getbankSlider" className="slider slide" >
			<Slider  {...settings}>
			  <div >
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner top-caption">
					  <span className="subcaption">Zero-fee checking account<sup className="small">SM</sup></span>
					  <h2>Get more bank for <br className="mobile" />your buck</h2>
					  <p>With zero-fee checking<sup className="small">SM</sup> from MoneyLion, you'll never be nickel and dimed by your bank again.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_1} alt="..." className="desktop" /> </div>
			  </div>
			  <div >
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="subcaption">Zero-fee investment account<sup className="small">SM</sup></span>
					  <h2>Invest like the 1% - <br className="mobile" />only easier</h2>
					  <p>Invest your money effortlessly in a managed investment account that’s personalized for you.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_2} alt="..." className="desktop" /> </div>
			  </div> 
			  <div >
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="subcaption">5.99% APR loans in seconds</span>
					  <h2>Get your loan in <br className="mobile" />seconds, not days</h2>
					  <p>Cover unexpected expenses with $500 5.99% APR loans, even if you don’t have great credit.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_3} alt="..." className="desktop" />  </div>
			  </div>
			  <div >
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="subcaption">0% APR instacash<sup className="small">SM</sup> cash advances</span>
					  <h2>Get paid any day, <br className="mobile" />your way</h2>
					  <p>Need to top off your account? Get a 0% APR cash advance in seconds, even if it’s 5 a.m. on a Sunday.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_4} alt="..." className="desktop" />  </div>
			  </div>
			  <div >
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="comingSoonContainer">COMING SOON</span><br />
					  <span className="subcaption">Up to 12% cashback rewards<sup className="small">SM</sup></span>
					  <h2>GET MORE FROM YOUR <br className="mobile" />MONEY</h2>
					  <p>Earn up to 12% cashback rewards on everyday purchases at thousands of your favorite national brands.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_5} alt="..." className="desktop" /> </div>
			  </div>
			  <div >
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="comingSoonContainer">COMING SOON</span><br />
					  <span className="subcaption">More to come</span>
					  <h2>EXPECT THE BEST</h2>
					  <p>Stay tuned for new features that will spoil you rotten. We're always working to make your membership more powerful than ever, because you deserve the best.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_6} alt="..." className="desktop" />  </div>
			  </div>
			 
			  
			</Slider>
		</div>
		</div>
		<div id="getbankSlider" className="carousel slide mobile" data-ride="carousel">
			<div className="carousel-inner">
			  <div className="carousel-item active">
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner top-caption">
					  <span className="subcaption">Zero-fee checking account<sup className="small">SM</sup></span>
					  <h2>Get more bank for <br className="mobile" />your buck</h2>
					  <p>With zero-fee checking<sup className="small">SM</sup> from MoneyLion, you'll never be nickel and dimed by your bank again.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_1} alt="..." className="desktop" /> <img src={bank_slide_1_mob} alt="..." className="mobile shadow" /></div>
			  </div>
			  <div className="carousel-item">
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="subcaption">Zero-fee investment account<sup className="small">SM</sup></span>
					  <h2>Invest like the 1% - <br className="mobile" />only easier</h2>
					  <p>Invest your money effortlessly in a managed investment account that’s personalized for you.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_2} alt="..." className="desktop" /> <img src={bank_slide_2_mob} alt="..." className="mobile shadow" /> </div>
			  </div>
			  <div className="carousel-item">
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="subcaption">5.99% APR loans in seconds</span>
					  <h2>Get your loan in <br className="mobile" />seconds, not days</h2>
					  <p>Cover unexpected expenses with $500 5.99% APR loans, even if you don’t have great credit.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_3} alt="..." className="desktop" /> <img src={bank_slide_3_mob} alt="..." className="mobile shadow" /> </div>
			  </div>
			  <div className="carousel-item">
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="subcaption">0% APR instacash<sup className="small">SM</sup> cash advances</span>
					  <h2>Get paid any day, <br className="mobile" />your way</h2>
					  <p>Need to top off your account? Get a 0% APR cash advance in seconds, even if it’s 5 a.m. on a Sunday.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_4} alt="..." className="desktop" /> <img src={bank_slide_4_mob} alt="..." className="mobile shadow" /> </div>
			  </div>
			  <div className="carousel-item">
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="comingSoonContainer">COMING SOON</span><br />
					  <span className="subcaption">Up to 12% cashback rewards<sup className="small">SM</sup></span>
					  <h2>GET MORE FROM YOUR <br className="mobile" />MONEY</h2>
					  <p>Earn up to 12% cashback rewards on everyday purchases at thousands of your favorite national brands.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_5} alt="..." className="desktop" /> <img src={bank_slide_5_mob} alt="..." className="mobile shadow" /> </div>
			  </div>
			  <div className="carousel-item">
				<div className="w-50">
				  <div className="caption">
					<div className="caption-inner">
					  <span className="comingSoonContainer">COMING SOON</span><br />
					  <span className="subcaption">More to come</span>
					  <h2>EXPECT THE BEST</h2>
					  <p>Stay tuned for new features that will spoil you rotten. We're always working to make your membership more powerful than ever, because you deserve the best.</p>
					</div>
				  </div>
				</div>
				<div className="w-50"> <img src={bank_slide_6} alt="..." className="desktop" /> <img src={bank_slide_6_mob} alt="..." className="mobile shadow" /> </div>
			  </div>
			 
			  <a className="carousel-control-prev" href="#getbankSlider" role="button" data-slide="prev"> 
				<span className="icon-wrapper">
					<img src={arrow_left} alt="leftArrow" /> 
				</span> 
				<span className="sr-only">Previous</span>
			  </a> 
			  <a className="carousel-control-next" href="#getbankSlider" role="button" data-slide="next"> 
				  <span className="icon-wrapper">
					  <img src={arrow_right} alt="rightArrow" /> 
				  </span> 
				  <span className="sr-only">Next</span>
			  </a> 
			</div>
		</div>
	  </div>
	</div>
    );
  }
}


export default SliderWrap;
