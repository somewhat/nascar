import React, { Component } from 'react';
import MoneyLionGradient from './images/MoneyLionGradient@2x.png';

class TableWrap extends Component {
  tab_1 = (e) => {
	document.getElementById('tab-1').classList.add('active');
	document.getElementById('tab-2').classList.remove('active');
	document.getElementById('tab-3').classList.remove('active');
	
	document.getElementById('tableWrap').classList.add('tab-1');
	document.getElementById('tableWrap').classList.remove('tab-2');
	document.getElementById('tableWrap').classList.remove('tab-3');
	
  }
  tab_2 = (e) => {
    document.getElementById('tab-1').classList.remove('active');
	document.getElementById('tab-2').classList.add('active');
	document.getElementById('tab-3').classList.remove('active');
	
	document.getElementById('tableWrap').classList.remove('tab-1');
	document.getElementById('tableWrap').classList.add('tab-2');
	document.getElementById('tableWrap').classList.remove('tab-3');
	
  }
  tab_3 = (e) => {
    document.getElementById('tab-1').classList.remove('active');
	document.getElementById('tab-2').classList.remove('active');
	document.getElementById('tab-3').classList.add('active');
	
	document.getElementById('tableWrap').classList.remove('tab-1');
	document.getElementById('tableWrap').classList.remove('tab-2');
	document.getElementById('tableWrap').classList.add('tab-3');
	
  }
  render() {
    return (
	<div className="main-secton-table text-center">
		<div className="container">
			<h2 className="mb-5 title-text">See why we're like no one else</h2>
            <div className="table-tabs mobile">
            	<span id="tab-1" rel="2" className="active"  onClick={this.tab_1}><img src={MoneyLionGradient} className="money-icon" alt=""/></span>
                <span id="tab-2" rel="3" onClick={this.tab_2}><em>Traditional<br />banks</em></span>
                <span id="tab-3" rel="4" onClick={this.tab_3}>Other digital<br />banks</span>
            </div>
			<table className="table mb-5 tab-1" id="tableWrap">
				<thead className="text-left">
					<tr>
						<th>&nbsp;</th>
						<th><img src={MoneyLionGradient} className="money-icon" alt=""/><div>Like no one else</div></th>
						<th>Traditional banks<div>Like Chase Total Checking&reg;</div></th>
						<th>Other digital banks <div>Like Ally and Capital One</div></th>
					</tr>
				</thead>
				<tbody className="text-left">
					<tr>
						<td>Monthly service fee</td>
						<td>$0</td>
						<td>$12</td>
						<td>$0</td>
					</tr>
					<tr>
						<td>Minimum balance to<br /> avoid monthly fee</td>
						<td>$0</td>
						<td>$1500</td>
						<td>$0</td>
					</tr>
					<tr>
						<td>Overdraft fees</td>
						<td>$0</td>
						<td>$35</td>
						<td>$25</td>
					</tr>
					<tr>
						<td>ATM network</td>
						<td>55,000</td>
						<td>$5</td>
						<td>$0</td>
					</tr>

					<tr>
						<td>Access to cash advances</td>
						<td>Yes. 0% APR Instacash anytime <br /> before payday.</td>
						<td>No</td>
						<td>No</td>
					</tr>
					<tr>
						<td>Credit Builer loan</td>
						<td>$500 at 5.99% APR loans with<br /> MoneyLion Plus membership.<br />Good credit not required.</td>
						<td>No</td>
						<td>No</td>
					</tr>
					<tr>
						<td>Managed investment account</td>
						<td>Included. No fees*,<br /> No minimum investment.</td>
						<td>No</td>
						<td>No</td>
					</tr>
					<tr>
						<td>Rewards on spending</td>
						<td>Up to 12% cashback rewards.<br /> (coming soon)</td>
						<td>No</td>
						<td>No</td>
					</tr>
					<tr>
						<td>Free TransUnion® credit score and<br /> monitoring</td>
						<td>Yes. MoneyLion Plus members also<br /> get free weekly credit score<br /> updates.</td>
						<td>No</td>
						<td>No</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
    );
  }
}


export default TableWrap;
