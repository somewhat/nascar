import React, { Component } from 'react';
import './App.css';

import Header from './header';
import TopBanner from './top_banner';
import Membership from './membership';
import SliderWrap from './slider_wrap';
import Discover from './discover';
import PlanWrap from './plan_wrap';
import TableWrap from './table_wrap';
import HowStrongWrap from './how_strong_wrap';
import MapWrap from './map_wrap';
import FeaturedWrap from './featured_wrap';
import SecureWrap from './secure_wrap';
import Footer from './footer';

class App extends Component {
  handleParentClick = e => {
    document.getElementsByClassName('people-popup')[0].classList.remove('active');
	document.getElementsByClassName('people-popup')[1].classList.remove('active');
	document.getElementsByClassName('people-popup')[2].classList.remove('active');
	document.getElementsByClassName('people-popup')[3].classList.remove('active');
	document.getElementsByClassName('people-popup')[4].classList.remove('active');
  }
  render() {
    return (
      <div onClick={this.handleParentClick}>
        <Header />
		<TopBanner />
		<Membership />
		<SliderWrap />
		<Discover />
		<PlanWrap />
		<TableWrap />
		<HowStrongWrap />
		<MapWrap />
		<FeaturedWrap />
		<SecureWrap />
		<Footer />
	  </div>
    );
  }
  componentDidMount() {
    
  }
}


export default App;

