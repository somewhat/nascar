import React, { Component } from 'react';
import animation from './images/animation.png';
import watch_video_icon from './images/watchvideoIcon.png';

class Discover extends Component {
  handleClick = (e) => {
    document.getElementById('videopopup1').classList.add('show');
	document.getElementById('bannervideo1').setAttribute('src', "https://www.youtube.com/embed/Y9jmGubITIA");
	
  }
  videoWrapClick = (e) => {
    document.getElementById('videopopup1').classList.remove('show');
	document.getElementById('bannervideo1').setAttribute('src', "");
  }
  render() {
    return (
	<div className="section-descover-wrap">
	  <div className="container">
		<div className="section-descover">
		  <div className="row">
			<div className="col-md-6 text-right desktop">
			  <div className="animate-img"><img src={animation} alt="Mobile Anim" /></div>
			</div> 
			<div className="col-md-6 pt-5 pr-5 discovercontent"> 
				<span className="comingSoonContainer">COMING SOON</span>
				<h2>DISCOVER YOUR FINANCIAL HEARTBEAT</h2>
				<p>Millions of hardworking Americans deserve an easy way to take control and get ahead. So we built it.</p>
	
				<p>Introducing your Financial Heartbeat, a game-changing way to monitor four key areas of your financial health —<strong> Save, Spend, Shield,</strong> and <strong>Score</strong> – and celebrate progress as you <strong>Strive</strong>.</p>
	
				<p>All in the palm of your hand, all for free.</p>
				
				<span className="watchvideo" data-toggle="modal" onClick={this.handleClick}><img src={watch_video_icon} alt="playIcon" />Watch Video</span>
			  </div>
			 <div className="col-md-6 text-right mobile">
			  <div className="animate-img"><img src={animation} alt="Mobile Anim" /></div>
			</div> 
			  <div id="videopopup1" className="modal" onClick={this.videoWrapClick}>
				<div className="modal-dialog">
					<div className="modal-content">
						<div className="modal-body">
							<iframe id="bannervideo1" width="560" height="315" src="" frameBorder="0" allowFullScreen title="frame-1" ></iframe>
						</div>
					</div>
				</div>
			</div>
			</div>
		  </div>
		</div>
	  </div>
    );
  }
}


export default Discover;
