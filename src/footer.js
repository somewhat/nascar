import React, { Component } from 'react';
import google_btn from './images/google-btn.png';
import app_btn from './images/app-btn.png';
import fb_icon from './images/facebook.png';
import li_icon from './images/linkedin.png';
import tw_icon from './images/twitter.png';
import footer_icon from './images/lionheadf.png';


class Footer extends Component {
  render() {
    return (
      <div className="main-footer text-left">
		 <div className="container">
			 <div className="row mb-5">
				<div className="col-sm-12 col-md-4">
					<h5 className="ftr-bold-text">CONTACT</h5>
					<ul className="address">
						<li>MoneyLion, Inc.</li>
						<li>P.O. Box 1247</li>
						<li>Sandy, UT 84091-1547</li>
						<li>1-888-704-6970</li>
						<li><a className="link-0-424" href="mailto:support@moneylion.com">support@moneylion.com</a></li>
					</ul>
				</div>
		
				<div className="col-sm-12 col-md-2 footer-float-left">
					<h5 className="ftr-bold-text">COMPANY</h5>
					<ul>
						<li><a href="https://www.moneylion.com/about" traget="_blank" rel="noopener noreferrer">About</a></li>
						<li><a href="https://blog.moneylion.com/" traget="_blank" rel="noopener noreferrer">Blog</a></li>
						<li><a href="https://support.moneylion.com/" traget="_blank" rel="noopener noreferrer">Support</a></li>
						<li><a href="https://www.moneylion.com/legal" traget="_blank" rel="noopener noreferrer">Legal</a></li>
					</ul>
				</div>
				<div className="col-sm-12 col-md-3 footer-float-left">
					<h5 className="ftr-bold-text">Products</h5>
					<ul>
						<li><a href="https://www.moneylion.com/plus/" traget="_blank" rel="noopener noreferrer"><strong>MoneyLion <span className="color-yellow"> plus</span></strong></a></li>
						<li><a href="https://www.moneylion.com/plus/borrowing" traget="_blank" rel="noopener noreferrer">Personal Loans</a></li>
						<li><a href="https://www.moneylion.com/free-credit-monitoring" traget="_blank" rel="noopener noreferrer">Credit Monitoring</a></li>
					</ul>
				</div>
		
				<div className="col-sm-12 col-md-3">
					<h5 className="ftr-bold-text">Financial Health</h5>
					<ul>
						<li><a href="https://www.moneylion.com/plus/investing" traget="_blank" rel="noopener noreferrer">Investing and Planning</a></li>
						<li><a href="https://www.moneylion.com/rewards" traget="_blank" rel="noopener noreferrer">Points and Rewards</a></li>
					</ul>
				</div>
			  </div>
		
			 <div className="icon-footer mb-3">
				<div className="row">
					<div className="col-sm-12 col-md-6 ftr-wrap">
						<ul>
							<li><a href="https://bnc.lt/E1yh/4FtmTYT4MJ" traget="_blank" rel="noopener noreferrer"><img src={google_btn} alt=""/></a></li>
							<li><a href="https://bnc.lt/E1yh/4FtmTYT4MJ" traget="_blank" rel="noopener noreferrer"><img src={app_btn} alt=""/></a></li>
						</ul>
					</div>
					<div className="mob-divider"></div>
					<div className="col-sm-12 col-md-6 ftr-social text-right">
						<ul>
							<li><a href="https://www.facebook.com/moneylion" traget="_blank" rel="noopener noreferrer"><img src={fb_icon} alt=""/></a></li>
							<li><a href="https://www.linkedin.com/company/moneylion" traget="_blank" rel="noopener noreferrer"><img src={li_icon} alt=""/></a></li>
							<li><a href="https://twitter.com/moneylion" traget="_blank" rel="noopener noreferrer"><img src={tw_icon} alt=""/></a></li>
						</ul>
					</div>
				</div>
			</div>
			<hr />
		
			<div className="ftr-lower text-left mt-5">
				<div className="row">
					<div className="col-sm-12 col-md-4">
						<div className="my-3s"><img src={footer_icon} alt=""/></div>

							  <div className="footer-copyright py-3">&copy; 2013 - 2018, MoneyLion, inc.<br />
								<div>All Rights Reserved.</div>
							  </div>

		
					</div>
		
					<div className="col-sm-12 col-md-7 ftr-text-grp">
						<div>
							<p>* Broker-Dealer charges a $0.25 withdrawal charge.</p>
							<p>
								MoneyLion membership required to participate in program, services and loans. Identity and account verification required. View membership terms <a href="https://www.moneylion.com/plus/membership"  traget="_blank" rel="noopener noreferrer">here</a>. 
							</p>
							<p>
								Investment Accounts Are Not FDIC Insured • No Bank Guarantee • May Lose Value • Membership Required. For important information and disclaimers relating to the Plus investment account, please <a href="https://moneylion.helpshift.com/a/moneylion-plus-1518215303/?p=web&s=moneylion-plus-investment-account"  traget="_blank" rel="noopener noreferrer">click here</a>. 
							</p>
							<p>
								All loans in the USA with an APR of 5.99% are made by state-licensed subsidiaries. Loan terms range from 6 - 18 months with 0% origination fees. An example of total amount paid on a personal loan of $500 for a term of 12 months at an APR of 5.99% with 0% origination fees, would be equivalent to $515.07 over the 12 month life of the loan.
		
							</p>
		
		
		
						</div>
						<div>
							MoneyLion’s affiliate, ML Wealth LLC, is a Registered Investment Adviser with the SEC. MoneyLion Plus members’ investments are protected by the SIPC (Securities Investor Protection Corporation) for up to $500,000. Brokerage services are provided to clients of ML Wealth by Drive Wealth LLC, a member of FINRA/SIPC. Other services are provided by affiliates, including ML Plus, LLC. This communication and all data are for informational purposes only and do not constitute a recommendation to buy or sell securities. Past performance is no guarantee of future results. Keep in mind that investing involves risk. The value of your investment will fluctuate over time, and you may gain or lose money. Nothing in this communication should be construed as an offer, recommendation, or solicitation to buy or sell any securities or services. The statement that MoneyLion offers “America’s Most Powerful Financial Membership” is based on MoneyLion’s own evaluation of the overall range of services and features the MoneyLion Plus membership offers as compared to those offered by membership programs sponsored by other financial institutions.  In reaching this conclusion, MoneyLion did not limit its assessment exclusively to investment advisory services.  It considered the full scope of services and features offered, including investment advisory capabilities, cashback features, rewards points, overdraft fees, access to ATMs, cash advances, consumer loans, annual percentage rates (APR), and underwriting criteria, in relation to the amount of the monthly membership or user fee.
						</div>
					</div>
				</div>
			 </div>
		</div>
		</div>
    );
  }
}


export default Footer;
