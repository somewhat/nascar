import React, { Component } from 'react';
import wall_street_journal from './images/wall-street-journal.png';
import cnn_money from './images/cnn-money.png';
import huffpost from './images/huffpost.png';
import forbes from './images/forbes.png';
import techcrunch_trans from './images/techcrunch_trans.png';
import furture from './images/furture.png';

class FeaturedWrap extends Component {
  render() {
    return (
	<div className="sectoin-featured text-center">
	  	<div className="container my-5">
	  		<h2 className="featuredin">FEATURED IN</h2>
	  		<div className="row mt-5">
	  			<div className="col-md-2 sec-mr icons"><img className="featured-icon" src={wall_street_journal} alt="" /></div>
	  			<div className="col-md-2"><img src={cnn_money} alt="" /></div>
	  			<div className="col-md-2 sec-mr"><img  src={huffpost}  alt="" /></div>
	  			<div className="col-md-2"><img src={forbes} alt="" /></div>
	  			<div className="col-md-2 sec-mr"><img src={techcrunch_trans}  alt=""/></div>
	  			<div className="col-md-2"><img src={furture}  alt=""/></div>
	  			</div><br className="desktop" /><br className="desktop" /><hr />
	  	</div>
		</div>
    );
  }
}


export default FeaturedWrap;
