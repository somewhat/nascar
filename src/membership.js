import React, { Component } from 'react';

class Membership extends Component {
  render() {
    return (
	<div>
		<div className="section-membership">
		  <div className="container">
			<h2 className="text-center header">MoneyLion Racing fans get</h2>
			<div className="row text-center">
			  <div className="col-md-3 text-center">
				<h2 className="display-2">1,500+</h2>
				<div className="description">Free Tickets</div>
			  </div>
			  <div className="col-md-3 text-center">
				<h2 className="display-2 pl40">5 <sup>%</sup></h2>
				<div  className="description">Cash Back on tickets to the track</div>
			  </div>
			  <div className="col-md-3 text-center">
				  <h2 className="display-2 pl40">5 <sup>%</sup></h2>
				<div  className="description">Cash Back on <br />NASCAR.com purchases</div>
			  </div>
			  <div className="col-md-3 text-center">
				<div>
					<h2 className="display-2 pl40">5 <sup>%</sup></h2>
				</div>
				<div  className="description">Cash Back on all at-<br />track purchases</div>
			  </div>
			</div>
		  </div>
		</div>
		<div className="section-membership white">
		  <div className="container">
			<h2 className="text-center header">Plus, the financial membership that has it all</h2>
			<div className="row text-center">
			  <div className="col-md-3 text-center">
				<h2 className="display-2"><sup>$</sup>0</h2>
				<div className="description">Zero-fee checking<sup>SM</sup></div>
				<div className="description">Zero-fee managed investing<sup className="superText">SM</sup></div>
			  </div>
			  <div className="col-md-3 text-center">
				<h2 className="display-2 pl40">12 <sup>%</sup></h2>
				<div  className="description">Up to 12%</div>
				<div  className="description">cashback rewards<sup className="superText">SM</sup></div>
			  </div>
			  <div className="col-md-3 text-center">
				  <h2 className="display-2">0 <sup className="supapr"><div>%</div><div className="apr">APR</div></sup></h2>
				<div  className="description">Instacash<sup>SM</sup></div>
				<div  className="description">cash advances</div>
			  </div>
			  <div className="col-md-3 text-center">
				<div>
					<h2 className="display-2">5.99 <sup className="supapr"><div>%</div><div className="apr">APR</div></sup></h2>
				</div>
				<div  className="description">Low APR <br/>personal loans</div>
			  </div>
			</div>
		  </div>
		</div>
	</div>
    );
  }
}


export default Membership;
