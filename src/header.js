import React, { Component } from 'react';
import logo from './images/logo-1.png';

class Header extends Component {
  render() {
    return (
	<div className="main-header">
	  <div className="container">
		<div className="header customheader">
		  <div className="row">
			  <div className="col-md-6 header-logo"> <a href="https://www.moneylion.com/" tagget="_blank" rel="noopener noreferrer"><img className="header-icon-width" src={logo} alt="logo" /></a></div>
			<div className="col-md-6 header-btn">
			  <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" rel="noopener noreferrer" className="btn btn-info px-5">Join Now</a>
			</div>
		  </div>
		</div>
	  </div>
	</div>
    );
  }
}


export default Header;
