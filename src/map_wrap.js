import React, { Component } from 'react';


class MapWrap extends Component {
  
  handleClick = param => e => {

    e.stopPropagation();
	
   	document.getElementsByClassName('people-popup')[0].classList.remove('active');
	document.getElementsByClassName('people-popup')[1].classList.remove('active');
	document.getElementsByClassName('people-popup')[2].classList.remove('active');
	document.getElementsByClassName('people-popup')[3].classList.remove('active');
	document.getElementsByClassName('people-popup')[4].classList.remove('active');
	document.getElementById(param).classList.add('active');
  }
  
  render() {
    return (
	<div className="bottomContainer-0-327 container-0-69 show-0-71">
		<div className="main-banner">
		  <div className="people people-wrapper move-to-position">
			<div  onClick={this.handleClick('people-1')} className="people people-1 interactive">
			  <div className="people-photo"></div>
			  <div className="balloon"></div>
			  <div id="people-1" className="people-popup position-right">
				<div className="name">Kara D.</div>
				<div className="feedback">"I'm investing while borrowing at 5.99%! So easy to save. Plus I got $25 gift cards!" *</div>
				<div className="people-corner-wrapper">
				  <div className="corner"></div>
				</div>
			  </div>
			</div>
			<div  onClick={this.handleClick('people-2')} className="people people-2 interactive">
			  <div className="people-photo"></div>
			  <div className="balloon"></div>
			  <div id="people-2" className="people-popup position-right">
				<div className="name">Nicole T.</div>
				<div className="feedback">"My credit score went up 47 points, and I have $200 in my investment account after 2 months!" *</div>
				<div className="people-corner-wrapper">
				  <div className="corner"></div>
				</div>
			  </div>
			</div>
			<div  onClick={this.handleClick('people-3')} className="people people-3 interactive">
			  <div className="people-photo"></div>
			  <div className="balloon"></div>
			  <div id="people-3" className="people-popup position-left">
				<div className="name">Matt W.</div>
				<div className="feedback">"I got a loan deposited fast, and I have savings automatically invested for me!" *</div>
				<div className="people-corner-wrapper">
				  <div className="corner"></div>
				</div>
			  </div>
			</div>
			<div  onClick={this.handleClick('people-4')} className="people people-4 interactive">
			  <div className="people-photo"></div>
			  <div className="balloon"></div>
			  <div id="people-4" className="people-popup position-left">
				<div className="name">Stacey K.</div>
				<div className="feedback">"My credit score went up 40 points so far, and I got a $25 gift card." *</div>
				<div className="people-corner-wrapper">
				  <div className="corner"></div>
				</div>
			  </div>
			</div>
			<div onClick={this.handleClick('people-5')} className="people people-5 interactive">
			  <div className="people-photo"></div>
			  <div className="balloon"></div>
			  <div id="people-5" className="people-popup position-left">
				<div className="name">Joseph C.</div>
				<div className="feedback">"My biggest thrill of the day is getting my $1 cashback bonus! My credit has improved too." *</div>
				<div className="people-corner-wrapper">
				  <div className="corner"></div>
				</div>
			  </div>
			</div>
		  </div>
		</div>
	</div>
    );
  }
}


export default MapWrap;
