import React, { Component } from 'react';
import logo from './images/logo-1.png';
import logo_2 from './images/logo-2.png';

class Header extends Component {
  render() {
    return (
	<div className="main-header">
	  <div className="container">
		<div className="header customheader">
		  <div className="row">
			  <div className="col-md-6 header-logo"> <a href="#"><img className="header-icon-width" src={logo} alt="logo" /></a><span className="xmark">x</span><a href="#"><img className="header-icon-width" src={logo_2} alt="logo" /></a> </div>
			<div className="col-md-6 header-btn">
			  <a href="https://bnc.lt/E1yh/4FtmTYT4MJ" target="_blank" className="btn btn-info px-5">Join For Free</a>
			</div>
		  </div>
		</div>
	  </div>
	</div>
    );
  }
}


export default Header;
